package com.visionapps.demo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.visionapps.demo.view.grid.GridItemsProvider;
import com.visionapps.grid.R;

/**
 * Simple items provider for demo purposes.
 */
public class DemoItemsProvider implements GridItemsProvider {

    // ------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    // ------------------------------------------------------------------------
    public DemoItemsProvider(Context context, int rows, int columns) {
        mInflater = LayoutInflater.from(context);
        initItems(rows, columns);
    }

    // ------------------------------------------------------------------------
    //
    // FIELDS
    //
    // ------------------------------------------------------------------------
    private final LayoutInflater mInflater;
    private DemoItem[][] mItems;

    // ------------------------------------------------------------------------
    //
    // METHODS
    //
    // ------------------------------------------------------------------------
    @Override
    public int getRowsCount() {
        return mItems.length;
    }

    @Override
    public int getColumnsCount() {
        return mItems[0].length;
    }

    @Override
    public int getColumnWidth(int column) {
        return 600;
    }

    @Override
    public int getRowHeight(int row) {
        return 300;
    }

    @Override
    public View getItemView(int row, int column, View cachedView, ViewGroup parent) {
        ViewHolder holder;
        if (cachedView == null) {
            cachedView = mInflater.inflate(mItems[row][column].viewResourceId, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) cachedView.findViewById(R.id.grid_item_title);
            cachedView.setTag(holder);
        } else {
            holder = (ViewHolder) cachedView.getTag();
        }

        holder.title.setText(String.valueOf(getItemId(row, column)));
        return cachedView;
    }

    @Override
    public Object getItemId(int row, int column) {
        return mItems[row][column].id;
    }

    @Override
    public Object getItemViewType(int row, int column) {
        return mItems[row][column].viewResourceId;
    }

    private void initItems(int rowsCount, int columnsCount) {
        mItems = new DemoItem[rowsCount][columnsCount];
        for (int row = 0; row < rowsCount; row++) {
            mItems[row] = new DemoItem[columnsCount];
            for (int column = 0; column < columnsCount; column++) {
                mItems[row][column] = new DemoItem();
                mItems[row][column].id = String.format("%s:%s", row, column);
                mItems[row][column].viewResourceId = (row + column) % 2 == 0 ? R.layout.grid_item_white : R.layout.grid_item_grey;
            }
        }
    }

    // ------------------------------------------------------------------------
    //
    // INNER DECLARATIONS
    //
    // ------------------------------------------------------------------------

    private static class ViewHolder {
        public TextView title;
    }

    private class DemoItem {
        public Object id;
        public int viewResourceId = R.layout.grid_item_white;
    }

}
