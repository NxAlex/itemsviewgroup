package com.visionapps.demo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.*;
import android.widget.OverScroller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * ViewGroup to render a collection of items with support of views recycling, scrolling, flinging, etc.
 *
 * @param <ITEM> Type of the items to be rendered.
 */
public abstract class ItemsViewGroup<ITEM extends ItemsViewGroup.Item> extends ViewGroup {

    // ------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    // ------------------------------------------------------------------------
    public ItemsViewGroup(Context context) {
        this(context, null, 0);
    }

    public ItemsViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ItemsViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    // ------------------------------------------------------------------------
    //
    // FIELDS
    //
    // ------------------------------------------------------------------------
    /**
     * Collection of items that are actually attached. Maps item id to item.
     */
    private final Map<Object, ITEM> mAttachedItems = new HashMap<Object, ITEM>();

    /**
     * X axis offset of the 1st item's top left corner.
     * At the start it is 0, but when you scroll of fling it's changed accordingly.
     */
    protected int offsetX;

    /**
     * Y axis offset of the 1st item's top left corner.
     * At the start it is 0, but when you scroll of fling it's changed accordingly.
     */
    protected int offsetY;

    /**
     * Item that was touched and you may want to consider it as a target for item tap gesture.
     */
    private ITEM mTouchedItem;

    /**
     * The gesture listener, used for handling simple gestures such as double touches, scrolls, flings.
     */
    private final GestureDetector.SimpleOnGestureListener mGestureListener = new GestureDetector.SimpleOnGestureListener() {

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            offsetBy(-distanceX, -distanceY);
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            fling((int) -velocityX, (int) -velocityY);
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            // if touch down during fling animation - reset touched item so it wouldn't be handled as item tap
            mTouchedItem = mScroller.computeScrollOffset() ? null : getTouchedItem(e.getX(), e.getY());
            mScroller.forceFinished(true);
            postInvalidateOnAnimation();

            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (mTouchedItem == null) return true;

            handleItemTap(mTouchedItem);
            mTouchedItem = null;
            return true;
        }
    };
    private ViewsCache mViewsCache;
    private GestureDetector mGestureDetector;
    private OverScroller mScroller;
    private OnItemClickListener<ITEM> mOnItemClickListener;

    // ------------------------------------------------------------------------
    //
    // METHODS
    //
    // ------------------------------------------------------------------------
    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        super.onTouchEvent(event);
        return mGestureDetector.onTouchEvent(event);
    }

    @Override
    public void computeScroll() {
        super.computeScroll();

        // offset content if the scroller fling is still active
        if (mScroller.computeScrollOffset()) {
            offsetBy(-offsetX - mScroller.getCurrX(), -offsetY - mScroller.getCurrY());
        }
    }

    @Override
    protected int computeHorizontalScrollOffset() {
        return -offsetX;
    }

    @Override
    protected int computeVerticalScrollOffset() {
        return -offsetY;
    }

    /**
     * Register a callback to be invoked when an item in this view has been clicked.
     *
     * @param listener The callback that will be invoked.
     */
    public void setOnItemClickListener(OnItemClickListener<ITEM> listener) {
        mOnItemClickListener = listener;
    }

    /**
     * Reposition and redraw items. You may want to call this method after the data provider changes.
     */
    public void redrawItems() {
        removeAllItems();
        layoutItems();
        invalidate();
    }

    /**
     * Subclasses must implement this method to layout their items.
     */
    protected abstract void layoutItems();

    /**
     * @param type Type of the view to obtain.
     * @return Cached view of the specified type or null if there is no such view.
     */
    protected View getCachedView(Object type) {
        return mViewsCache.get(type);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        layoutItems();
    }

    /**
     * @param itemId Item id.
     * @return true if the specified item is attached to the view, false - if not.
     */
    protected boolean isItemAttached(Object itemId) {
        return mAttachedItems.containsKey(itemId);
    }

    /**
     * Add new item and its view to the layout.
     *
     * @param item   Item to add.
     * @param left   Left position, relative to parent.
     * @param top    Top position, relative to parent.
     * @param width  Item view width.
     * @param height Item view height.
     */
    protected void addItem(ITEM item, int left, int top, int width, int height) {
        mAttachedItems.put(item.id, item);
        addViewToLayout(item.view);
        measureItemView(item.view, width, height);
        item.view.layout(left, top, left + width, top + height);
        item.view.invalidate();
    }

    private void measureItemView(final View view, final int width, final int height) {
        final int widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        final int heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        view.measure(widthMeasureSpec, heightMeasureSpec);
    }

    private void addViewToLayout(final View view) {
        LayoutParams params = view.getLayoutParams();
        if (params == null) params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        addViewInLayout(view, -1, params, true);
    }

    private void removeItemFromLayout(ITEM item) {
        mViewsCache.put(item.type, item.view);
        removeViewInLayout(item.view);
        item.view = null;
    }

    private void update() {
        removeInvisibleItems();
        awakenScrollBars();
        layoutItems();
        invalidate();
    }

    private void removeAllItems() {
        for (ITEM item : mAttachedItems.values()) {
            removeViewInLayout(item.view);
            mViewsCache.put(item.type, item.view);
        }
        mAttachedItems.clear();
    }

    /**
     * Offset all children views, so user will see an illusion of content scrolling or flinging.
     *
     * @param offsetDeltaX Offset X delta.
     * @param offsetDeltaY Offset Y delta.
     */
    private void offsetBy(float offsetDeltaX, float offsetDeltaY) {
        // adjust offset values
        final int adjustedOffsetDeltaX = adjustOffsetDelta(offsetX, offsetDeltaX, getRightOffsetBounds());
        final int adjustedOffsetDeltaY = adjustOffsetDelta(offsetY, offsetDeltaY, getBottomOffsetBounds());

        // offset views
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).offsetLeftAndRight(adjustedOffsetDeltaX);
            getChildAt(i).offsetTopAndBottom(adjustedOffsetDeltaY);
        }

        // update state
        offsetX += adjustedOffsetDeltaX;
        offsetY += adjustedOffsetDeltaY;
        update();
    }

    private void removeInvisibleItems() {
        final Iterator<Map.Entry<Object, ITEM>> iterator = mAttachedItems.entrySet().iterator();
        ITEM item;
        while (iterator.hasNext()) {
            item = iterator.next().getValue();
            if (isViewInvisible(item.view)) {
                iterator.remove();
                removeItemFromLayout(item);
            }
        }
    }

    /**
     * Check is view (at least one pixel of it) is inside the visible area or not.
     *
     * @param view View to check is it visible or not.
     * @return true if the view is invisible i.e. out of visible screen bounds, false - if at least one pixel of it is visible.
     */
    private boolean isViewInvisible(View view) {
        return view == null ||
                view.getLeft() >= getMeasuredWidth() ||
                view.getRight() <= 0 ||
                view.getTop() >= getMeasuredHeight() ||
                view.getBottom() <= 0;
    }

    private void init(Context context) {
        mViewsCache = new ViewsCache();
        mGestureDetector = new GestureDetector(getContext(), mGestureListener);
        mScroller = new OverScroller(context);
    }

    private void fling(int velocityX, int velocityY) {
        mScroller.forceFinished(true);
        mScroller.fling(-offsetX, -offsetY, velocityX, velocityY, 0, getRightOffsetBounds(), 0, getBottomOffsetBounds());
        invalidate();
    }

    private void handleItemTap(final ITEM item) {
        if (item.view == null || mOnItemClickListener == null) return;

        item.view.setPressed(true);

        // post a runnable that resets pressed state after a short duration so user will see a pressed state change
        postDelayed(new Runnable() {
            @Override
            public void run() {
                item.view.setPressed(false);
                mOnItemClickListener.onItemClick(ItemsViewGroup.this, item);
            }
        }, ViewConfiguration.getPressedStateDuration());
    }

    /**
     * Adjust requested offset delta to don't allow move content outside of the content bounds.
     *
     * @param currentOffset    Current offset.
     * @param offsetDelta      Desired offset delta to apply.
     * @param maxAllowedOffset Max allowed offset to left. Usually it's difference between content size and view size.
     * @return Adjusted offset delta.
     */
    private int adjustOffsetDelta(int currentOffset, float offsetDelta, int maxAllowedOffset) {
        // if view content size is smaller than the view size, offset is 0, i.e. we can't offset the content
        if (maxAllowedOffset < 0) return 0;

        // limit offset for top and left edges
        if (currentOffset + offsetDelta > 0) return -currentOffset;

        // limit offset for bottom and right edges
        if (maxAllowedOffset + currentOffset + offsetDelta < 0) return -(maxAllowedOffset + currentOffset);

        return (int) offsetDelta;
    }

    private int getBottomOffsetBounds() {
        return computeVerticalScrollRange() - getMeasuredHeight();
    }

    private int getRightOffsetBounds() {
        return computeHorizontalScrollRange() - getMeasuredWidth();
    }

    /**
     * Find item that was touched.
     *
     * @param touchX X coordinate of touch.
     * @param touchY Y coordinate of touch.
     * @return Touched item or null if nothing was found.
     */
    private ITEM getTouchedItem(float touchX, float touchY) {
        for (ITEM item : mAttachedItems.values()) {
            if (touchX > item.view.getLeft() && touchX < item.view.getRight() && item.view.getTop() < touchY && item.view.getBottom() > touchY) {
                return item;
            }
        }
        return null;
    }

    // ------------------------------------------------------------------------
    //
    // INNER DECLARATIONS
    //
    // ------------------------------------------------------------------------

    /**
     * Interface definition for a callback to be invoked when an item in this view has been clicked.
     */
    public interface OnItemClickListener<ITEM> {
        /**
         * Invoked when an item was clicked.
         *
         * @param parent The parent view where the click happened.
         * @param item   Item that was clicked.
         */
        public void onItemClick(ItemsViewGroup parent, ITEM item);
    }

    /**
     * Describes a single item to be rendered.
     */
    public static class Item {

        /**
         * View rendered for item.
         */
        public View view;

        /**
         * Item view type.
         */
        public Object type;

        /**
         * Item id.
         */
        public Object id;

    }

}
