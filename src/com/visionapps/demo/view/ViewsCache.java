package com.visionapps.demo.view;

import android.view.View;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * View cache to simplify views reusing.
 */
public class ViewsCache {

    // ------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    // ------------------------------------------------------------------------
    /**
     * Constructs a new instance of views cache of default size.
     */
    public ViewsCache() {
        this(100);
    }

    /**
     * Constructs a new instance of views cache.
     *
     * @param maxViewsPerType Max number of views per type to keep in cache.
     */
    public ViewsCache(int maxViewsPerType) {
        mViewsCache = new HashMap<Object, Queue<View>>();
        mMaxViewsPerType = maxViewsPerType;
    }

    // ------------------------------------------------------------------------
    //
    // FIELDS
    //
    // ------------------------------------------------------------------------
    /**
     * Max number of views to keep in cache per type.
     */
    private final int mMaxViewsPerType;

    /**
     * Maps views types to a collection of views of that type available for reusing.
     */
    private Map<Object, Queue<View>> mViewsCache;

    // ------------------------------------------------------------------------
    //
    // METHODS
    //
    // ------------------------------------------------------------------------
    /**
     * @param viewType Item view type.
     * @return A cached view available for reuse or null if nothing is available.
     */
    public View get(Object viewType) {
        final Queue<View> viewsCacheForType = mViewsCache.get(viewType);
        return viewsCacheForType == null ? null : viewsCacheForType.poll();
    }

    /**
     * Put a view into the cache.
     *
     * @param viewType Type of the view. It can be used later to retrieve a cached view for that type.
     * @param view     The view to cache.
     */
    public void put(Object viewType, final View view) {
        Queue<View> viewsCacheForType = mViewsCache.get(viewType);

        if (viewsCacheForType == null) {
            viewsCacheForType = new LinkedList<View>();
            mViewsCache.put(viewType, viewsCacheForType);
        }

        if (mViewsCache.size() < mMaxViewsPerType) viewsCacheForType.add(view);
    }

}
