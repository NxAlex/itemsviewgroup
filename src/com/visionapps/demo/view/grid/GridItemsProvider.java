package com.visionapps.demo.view.grid;

import android.view.View;
import android.view.ViewGroup;

/**
 * Acts as a bridge between a {@link GridView} and the underlying data for that view.
 */
public interface GridItemsProvider {

    // ------------------------------------------------------------------------
    //
    // METHODS
    //
    // ------------------------------------------------------------------------

    /**
     * @return Number of grid rows.
     */
    public int getRowsCount();

    /**
     * @return Number of grid columns.
     */
    public int getColumnsCount();

    /**
     * @param column Column index.
     * @return Column width.
     */
    public int getColumnWidth(int column);

    /**
     * @param row Row index.
     * @return Row height.
     */
    public int getRowHeight(int row);

    /**
     * Get view for the specified grid item.
     *
     * @param row        Item row index.
     * @param column     Item column index.
     * @param cachedView The old view to reuse if available, or null if there is no available cached view.
     * @param parent     The parent that this view will eventually be attached to.
     * @return Item view.
     */
    public View getItemView(int row, int column, View cachedView, ViewGroup parent);

    /**
     * Get the item id associated with the specified grid item.
     *
     * @param row    Item row index.
     * @param column Item column index.
     * @return Stable id of the item.
     */
    public Object getItemId(int row, int column);

    /**
     * Get the type of view that will be created by {@link #getItemView} for the specified item.
     *
     * @param row    Item row index.
     * @param column Item column index.
     * @return Type of the item view. Two views should share the same type if one can be converted to another in {@link #getItemView}.
     */
    public Object getItemViewType(int row, int column);

}
