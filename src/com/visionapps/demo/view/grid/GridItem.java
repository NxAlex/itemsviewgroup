package com.visionapps.demo.view.grid;

import com.visionapps.demo.view.ItemsViewGroup;

/**
 * Describes a single grid item.
 */
public class GridItem extends ItemsViewGroup.Item {

    // ------------------------------------------------------------------------
    //
    // FIELDS
    //
    // ------------------------------------------------------------------------

    /**
     * Item row index.
     */
    public int row;

    /**
     * Item column index.
     */
    public int column;

}
