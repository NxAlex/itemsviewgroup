package com.visionapps.demo.view.grid;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import com.visionapps.demo.view.ItemsViewGroup;
import com.visionapps.grid.R;

/**
 * A layout that arranges its children in a two-dimensional horizontally and vertically scrollable grid.
 * Grid rows and columns can have different sizes. Items views will match grid cells size.
 * Grid items and dimensions provided by {@link GridItemsProvider} implementation set by {@link #setData(GridItemsProvider)}.
 */
public class GridView extends ItemsViewGroup<GridItem> {

    // ------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    // ------------------------------------------------------------------------
    public GridView(Context context) {
        this(context, null, 0);
    }

    public GridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    // ------------------------------------------------------------------------
    //
    // FIELDS
    //
    // ------------------------------------------------------------------------
    protected GridItemsProvider data;

    /**
     * Padding around items.
     */
    protected int itemPadding;

    /**
     * X coordinate of the first visible row.
     */
    private int mFirstVisibleColumnX = 0;

    /**
     * Index of the first visible column.
     */
    private int mFirstVisibleColumn = 0;

    /**
     * Index of the first visible row.
     */
    private int mFirstVisibleRow = 0;

    /**
     * Y coordinate of the first visible row.
     */
    private int mFirstVisibleRowY = 0;

    /**
     * Total width of the grid with respect to all items.
     */
    private int mGridContentWidth = 0;

    /**
     * Total height of the grid with respect to all items.
     */
    private int mGridContentHeight = 0;

    // ------------------------------------------------------------------------
    //
    // METHODS
    //
    // ------------------------------------------------------------------------

    /**
     * Set new grid data.
     *
     * @param newData New grid data.
     */
    public void setData(GridItemsProvider newData) {
        data = newData;
        offsetX = offsetY = 0;
        redrawItems();
    }

    @Override
    public void redrawItems() {
        calculateContentSize();
        super.redrawItems();
    }

    /**
     * Layout items as a grid.
     */
    protected void layoutItems() {
        if (data == null) return;

        calculateFirstVisibleRow();
        calculateFirstVisibleColumn();

        int top = mFirstVisibleRowY + itemPadding, height;
        for (int row = mFirstVisibleRow; top < getMeasuredHeight() && row < data.getRowsCount(); row++) {
            height = getRowHeight(row);
            layoutRow(row, top, height);
            top += height + itemPadding;
        }
    }

    /**
     * Resolve height for the specified row.
     *
     * @param row Row index.
     * @return Height for the specified row.
     */
    protected int getRowHeight(int row) {
        return data.getRowHeight(row);
    }

    @Override
    protected int computeHorizontalScrollRange() {
        return mGridContentWidth;
    }

    @Override
    protected int computeVerticalScrollRange() {
        return mGridContentHeight;
    }

    /**
     * Resolve width for the specified column.
     *
     * @param column Column index.
     * @return Width for the specified column.
     */
    protected int getColumnWidth(int column) {
        return data.getColumnWidth(column);
    }

    /**
     * Create new item for the specified grid cell.
     *
     * @param row    Row index of the new item.
     * @param column Column index of the the item.
     * @return Created item.
     */
    private GridItem createItem(int row, int column) {
        final GridItem result = new GridItem();
        result.id = data.getItemId(row, column);
        result.row = row;
        result.column = column;
        result.type = data.getItemViewType(row, column);
        result.view = data.getItemView(row, column, getCachedView(result.type), this);

        return result;
    }

    /**
     * @param row    Row index.
     * @param top    Row top position.
     * @param height Row height.
     */
    private void layoutRow(int row, int top, int height) {
        int left = mFirstVisibleColumnX + itemPadding, width;
        for (int column = mFirstVisibleColumn; left < getMeasuredWidth() && column < data.getColumnsCount(); column++) {
            width = getColumnWidth(column);
            // skip attached items because they are already added and positioned
            if (!isItemAttached(data.getItemId(row, column))) {
                addItem(createItem(row, column), left, top, width, height);
            }
            left += width + itemPadding;
        }
    }

    private void init(Context context, AttributeSet attrs) {
        initFromAttributes(context, attrs);
    }

    /**
     * Calculate first visible column coordinates to minimize number of further iterations during items layout.
     */
    private void calculateFirstVisibleColumn() {
        mFirstVisibleColumn = 0;
        mFirstVisibleColumnX = offsetX;
        while (mFirstVisibleColumn < data.getColumnsCount() && mFirstVisibleColumnX + getColumnWidth(mFirstVisibleColumn) < 0) {
            mFirstVisibleColumnX += getColumnWidth(mFirstVisibleColumn++) + itemPadding;
        }
    }

    /**
     * Calculate first visible row coordinates to minimize number of further iterations during items layout.
     */
    private void calculateFirstVisibleRow() {
        mFirstVisibleRow = 0;
        mFirstVisibleRowY = offsetY;
        while (mFirstVisibleRow < data.getRowsCount() && mFirstVisibleRowY + getRowHeight(mFirstVisibleRow) < 0) {
            mFirstVisibleRowY += getRowHeight(mFirstVisibleRow++) + itemPadding;
        }
    }

    private void initFromAttributes(final Context context, final AttributeSet attrs) {
        if (attrs == null) return;

        final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.GridView);
        itemPadding = attributes.getDimensionPixelSize(R.styleable.GridView_itemPadding, 0);
        attributes.recycle();
    }

    /**
     * Calculate total grid content size with respect to rows/columns sizes and items padding.
     */
    private void calculateContentSize() {
        mGridContentWidth = 0;
        mGridContentHeight = 0;

        if (data == null) return;

        mGridContentWidth += itemPadding;
        for (int column = 0; column < data.getColumnsCount(); column++)
            mGridContentWidth += getColumnWidth(column) + itemPadding;

        mGridContentHeight += itemPadding;
        for (int row = 0; row < data.getRowsCount(); row++)
            mGridContentHeight += getRowHeight(row) + itemPadding;
    }

}
