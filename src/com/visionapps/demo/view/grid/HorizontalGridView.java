package com.visionapps.demo.view.grid;

import android.content.Context;
import android.util.AttributeSet;

/**
 * A layout that arranges its children in a horizontally scrollable grid.
 * Vertical space is evenly distributed among grid rows.
 * Grid items and dimensions provided by {@link GridItemsProvider} implementation set by {@link #setData(GridItemsProvider)}.
 * Grid rows and columns can have different sizes.
 */
public class HorizontalGridView extends GridView {

    // ------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    // ------------------------------------------------------------------------
    public HorizontalGridView(Context context) {
        this(context, null, 0);
    }

    public HorizontalGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HorizontalGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    // ------------------------------------------------------------------------
    //
    // METHODS
    //
    // ------------------------------------------------------------------------
    @Override
    protected int getRowHeight(int row) {
        return getMeasuredHeight() == 0 ?
                super.getRowHeight(row) :
                // calculate evenly distributed vertical space with respect to padding
                (getMeasuredHeight() - itemPadding) / data.getRowsCount() - itemPadding;
    }

    @Override
    protected int computeVerticalScrollRange() {
        return 0;
    }

}
