package com.visionapps.demo.view.grid;

import android.content.Context;
import android.util.AttributeSet;

/**
 * A layout that arranges its children in a vertically scrollable grid.
 * Horizontal space is evenly distributed among grid columns.
 * Grid items and dimensions provided by {@link GridItemsProvider} implementation set by {@link #setData(GridItemsProvider)}.
 */
public class VerticalGridView extends GridView {

    // ------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    // ------------------------------------------------------------------------
    public VerticalGridView(Context context) {
        this(context, null, 0);
    }

    public VerticalGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VerticalGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    // ------------------------------------------------------------------------
    //
    // METHODS
    //
    // ------------------------------------------------------------------------
    @Override
    protected int computeHorizontalScrollRange() {
        return 0;
    }

    @Override
    protected int getColumnWidth(int column) {
        return getMeasuredWidth() == 0 ?
                super.getColumnWidth(column) :
                // calculate evenly distributed horizontal space with respect to padding
                (getMeasuredWidth() - itemPadding) / data.getColumnsCount() - itemPadding;
    }

}
