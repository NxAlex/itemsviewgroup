package com.visionapps.demo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;
import com.visionapps.demo.view.ItemsViewGroup;
import com.visionapps.demo.view.grid.GridItem;
import com.visionapps.demo.view.grid.GridView;
import com.visionapps.grid.R;

import static com.visionapps.demo.view.ItemsViewGroup.OnItemClickListener;

public class DemoActivity extends Activity implements OnItemClickListener<GridItem> {

    // ------------------------------------------------------------------------
    //
    // METHODS
    //
    // ------------------------------------------------------------------------
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        GridView grid = (GridView) findViewById(R.id.activity_demo_grid);
        grid.setData(new DemoItemsProvider(this, 100, 100));
        grid.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(ItemsViewGroup parent, GridItem item) {
        Toast.makeText(this, item.id + " clicked", Toast.LENGTH_SHORT).show();
    }

}
